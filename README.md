# Cube3 Exercise - Tomer Peleg

The exercise for Cube3 by Tomer Peleg.

## Requirements

Node, Yarn.

## Start

After installing dependencies, run `gulp start`

## Notes

I didn't have time to complete the task unfortunately. I hope you'll be able to see from what is there and from the commit history what you need to know.
