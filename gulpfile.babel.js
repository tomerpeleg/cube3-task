import gulp from 'gulp'
import browserSync from 'browser-sync'
import sass from 'gulp-sass'
import postcss from 'gulp-postcss'
import autoprefixer from 'autoprefixer'
import cssnano from 'cssnano'
import sourcemaps from 'gulp-sourcemaps'
import htmlmin from 'gulp-htmlmin'

const bs = browserSync.create()

const config = {
    public: './public',

    styles: {
        src: 'src/scss/**/*.scss',
        dest: 'public/css',
        sourcemap: './',
        browsers: ['last 2 versions'],
    },

    markup: {
        src: 'src/html/**/*.html',
        dest: 'public',
    },

    assets: {
        src: 'src/assets/**/*',
        dest: 'public/assets',
    },
}

const plugins = [
    autoprefixer({ browsers: config.styles.browsers }),
    cssnano()
]

export function styles() {
    return gulp.src(config.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss(plugins))
        .pipe(sourcemaps.write(config.styles.sourcemap))
        .pipe(gulp.dest(config.styles.dest))
        .pipe(bs.stream({ match: '**/*.css' }))
}

export function markup() {
    return gulp.src(config.markup.src)
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(config.markup.dest))
}

export function assets() {
    return gulp.src(config.assets.src)
        .pipe(gulp.dest(config.assets.dest))
}

export function watch() {
    bs.init({
        server: {
            baseDir: config.public,
        }
    })

    gulp.watch(config.styles.src)
        .on('all', gulp.series(styles))
    gulp.watch(config.assets.src)
        .on('all', gulp.series(assets, bs.reload))
    gulp.watch(config.markup.src)
        .on('all', gulp.series(markup, bs.reload))
}


const build = gulp.parallel(styles, markup, assets)
const start = gulp.series(build, watch);

exports.build = exports.default = build
exports.start = start